This is the published version of the Materials Tribology Lab at KIT vocabulary.  
The file `metadata.json` contains the vocabulary's metadata as per VocPopuli's schema (in progress).  
 
 The vocabulary was created using [https://vocpopuli.com](https://vocpopuli.com).  
